# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-07-16 19:47+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Attribute 'lang' of: <appendix>
#: en/old-stuff.dbk:8
msgid "en"
msgstr ""

#. type: Content of: <appendix><title>
#: en/old-stuff.dbk:9
msgid "Managing your &oldreleasename; system before the upgrade"
msgstr ""

#. type: Content of: <appendix><para>
#: en/old-stuff.dbk:11
msgid ""
"This appendix contains information on how to make sure you can install or "
"upgrade &oldreleasename; packages before you upgrade to &releasename;.  This "
"should only be necessary in specific situations."
msgstr ""

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:16
msgid "Upgrading your &oldreleasename; system"
msgstr ""

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:18
msgid ""
"Basically this is no different from any other upgrade of &oldreleasename; "
"you've been doing.  The only difference is that you first need to make sure "
"your package list still contains references to &oldreleasename; as explained "
"in <xref linkend=\"old-sources\"/>."
msgstr ""

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:24
msgid ""
"If you upgrade your system using a Debian mirror, it will automatically be "
"upgraded to the latest &oldreleasename; point release."
msgstr ""

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:30
msgid "Checking your APT source-list files"
msgstr ""

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:32
msgid ""
"If any of the lines in your APT source-list files (see <ulink "
"url=\"&url-man;/&releasename;/apt/sources.list.5.html\">sources.list(5)</ulink>)  "
"contain references to <quote><literal>stable</literal></quote>, this is "
"effectively pointing to &releasename; already. This might not be what you "
"want if you are not yet ready for the upgrade.  If you have already run "
"<command>apt update</command>, you can still get back without problems by "
"following the procedure below."
msgstr ""

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:40
msgid ""
"If you have also already installed packages from &releasename;, there "
"probably is not much point in installing packages from &oldreleasename; "
"anymore.  In that case you will have to decide for yourself whether you want "
"to continue or not.  It is possible to downgrade packages, but that is not "
"covered here."
msgstr ""

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:46
msgid ""
"As root, open the relevant APT source-list file (such as "
"<filename>/etc/apt/sources.list</filename>) with your favorite editor, and "
"check all lines beginning with <literal>deb http:</literal>, <literal>deb "
"https:</literal>, <literal>deb tor+http:</literal>, <literal>deb "
"tor+https:</literal>, <literal>URIs: http:</literal>, <literal>URIs: "
"https:</literal>, <literal>URIs: tor+http:</literal> or <literal>URIs: "
"tor+https:</literal> for a reference to "
"<quote><literal>stable</literal></quote>.  If you find any, change "
"<literal>stable</literal> to <literal>&oldreleasename;</literal>."
msgstr ""

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:57
msgid ""
"If you have any lines starting with <literal>deb file:</literal> or "
"<literal>URIs: file:</literal>, you will have to check for yourself if the "
"location they refer to contains a &oldreleasename; or &releasename; archive."
msgstr ""

#. type: Content of: <appendix><section><important><para>
#: en/old-stuff.dbk:64
msgid ""
"Do not change any lines that begin with <literal>deb cdrom:</literal> or "
"<literal>URIs: cdrom:</literal>.  Doing so would invalidate the line and you "
"would have to run <command>apt-cdrom</command> again.  Do not be alarmed if "
"a <literal>cdrom:</literal> source line refers to "
"<quote><literal>unstable</literal></quote>.  Although confusing, this is "
"normal."
msgstr ""

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:73
msgid "If you've made any changes, save the file and execute"
msgstr ""

#. type: Content of: <appendix><section><screen>
#: en/old-stuff.dbk:76
#, no-wrap
msgid "# apt update\n"
msgstr ""

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:79
msgid "to refresh the package list."
msgstr ""

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:84
msgid "Removing obsolete configuration files"
msgstr ""

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:86
msgid ""
"Before upgrading your system to &releasename;, it is recommended to remove "
"old configuration files (such as <filename>*.dpkg-{new,old}</filename> files "
"under <filename>/etc</filename>) from the system."
msgstr ""

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:94
msgid "Upgrade legacy locales to UTF-8"
msgstr ""

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:96
msgid ""
"Using a legacy non-UTF-8 locale has been unsupported by desktops and other "
"mainstream software projects for a long time. Such locales should be "
"upgraded by running <command>dpkg-reconfigure locales</command> and "
"selecting a UTF-8 default. You should also ensure that users are not "
"overriding the default to use a legacy locale in their environment."
msgstr ""
