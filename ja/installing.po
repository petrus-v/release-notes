# Japanese translations for Debian release notes
# Debian リリースノートの日本語訳
# Noritada Kobayashi <nori1@dolphin.c.u-tokyo.ac.jp>, 2006.
# Nobuhiro IMAI, 2009.
# Hideki Yamane <henrich@debian.org>, 2010-2019.
# hoxp18 <hoxp18@noramail.jp>, 2019.
# This file is distributed under the same license as Debian release notes.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 9\n"
"Report-Msgid-Bugs-To: debian-doc@lists.debian.org\n"
"POT-Creation-Date: 2019-06-01 13:06+0900\n"
"PO-Revision-Date: 2019-06-15 17:26+0900\n"
"Last-Translator: Hideki Yamane <henrich@debian.org>\n"
"Language-Team: Japanese <debian-doc@debian.or.jp>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: Attribute 'lang' of: <chapter>
#: en/installing.dbk:8
msgid "en"
msgstr "ja"

#. type: Content of: <chapter><title>
#: en/installing.dbk:9
msgid "Installation System"
msgstr "インストールシステム"

#. type: Content of: <chapter><para>
#: en/installing.dbk:11
msgid ""
"The Debian Installer is the official installation system for Debian.  It "
"offers a variety of installation methods.  Which methods are available to "
"install your system depends on your architecture."
msgstr ""
"Debian Installer は公式の Debian インストールシステムです。このインストーラー"
"は、様々なインストール方法を提供しています。お使いのシステムにインストールす"
"るのにどの方法が利用できるかは、使っているアーキテクチャに依存します。"

#. type: Content of: <chapter><para>
#: en/installing.dbk:16
msgid ""
"Images of the installer for &releasename; can be found together with the "
"Installation Guide on the <ulink url=\"&url-installer;\">Debian website</"
"ulink>."
msgstr ""
"&releasename; 用のインストーラーのイメージは、インストールガイドとともに "
"<ulink url=\"&url-installer;\">Debian のウェブサイト</ulink>にあります。"

#. type: Content of: <chapter><para>
#: en/installing.dbk:21
msgid ""
"The Installation Guide is also included on the first media of the official "
"Debian DVD (CD/blu-ray) sets, at:"
msgstr ""
"インストールガイドは、Debian 公式 DVD セット (CD/blu-ray) の 1 枚目の、次の場"
"所にも含まれています。"

#. type: Content of: <chapter><screen>
#: en/installing.dbk:25
#, no-wrap
msgid "/doc/install/manual/<replaceable>language</replaceable>/index.html\n"
msgstr "/doc/install/manual/<replaceable>言語</replaceable>/index.html\n"

#. type: Content of: <chapter><para>
#: en/installing.dbk:28
msgid ""
"You may also want to check the <ulink url=\"&url-installer;index#errata"
"\">errata</ulink> for debian-installer for a list of known issues."
msgstr ""
"これまでに知られている問題点を列挙した debian-installer の<ulink url=\"&url-"
"installer;index#errata\">正誤表</ulink>も確認しておくとよいでしょう。"

#. type: Content of: <chapter><section><title>
#: en/installing.dbk:33
msgid "What's new in the installation system?"
msgstr "インストールシステムの変更点"

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:35
msgid ""
"There has been a lot of development on the Debian Installer since its "
"previous official release with &debian; &oldrelease;, resulting in improved "
"hardware support and some exciting new features or improvements."
msgstr ""
"Debian Installer は前回の &debian; &oldrelease; での公式リリース以降も活発に"
"開発されています。その結果、ハードウェアサポートが改善され、ワクワクするよう"
"な新機能や改善がいくつか追加されました。"

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:41
msgid ""
"Most notably there is the initial support for UEFI Secure Boot (see <xref "
"linkend=\"secure-boot\"/>), which has been added to the installation images."
msgstr ""
"中でも UEFI セキュアブート (<xref linkend=\"secure-boot\"/> をご覧ください) "
"への対応がインストールイメージに追加されています。"

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:45
msgid ""
"If you are interested in an overview of the detailed changes since "
"&oldreleasename;, please check the release announcements for the "
"&releasename; beta and RC releases available from the Debian Installer's "
"<ulink url=\"&url-installer-news;\">news history</ulink>."
msgstr ""
"&oldreleasename; 以降になされた詳細な変更の一覧に興味がある場合は、Debian "
"Installer の<ulink url=\"&url-installer-news;\">ニュースの履歴</ulink>で閲覧"
"可能な、&releasename; 用ベータ版やリリース候補版 (RC) のリリースアナウンスを"
"参照してください。"

#. type: Content of: <chapter><section><section><title>
#: en/installing.dbk:124
msgid "Automated installation"
msgstr "自動インストール"

#. type: Content of: <chapter><section><section><para>
#: en/installing.dbk:126
msgid ""
"Some changes mentioned in the previous section also imply changes in the "
"support in the installer for automated installation using preconfiguration "
"files.  This means that if you have existing preconfiguration files that "
"worked with the &oldreleasename; installer, you cannot expect these to work "
"with the new installer without modification."
msgstr ""
"前のセクションで述べたとおりインストーラーに多数の変更が加えられたので、インス"
"トーラーによる、事前設定ファイルを使用した自動インストールのサポートにも変更が"
"ありました。つまり、&oldreleasename; のインストーラーで動いた既存の事前設定"
"ファイルがあっても、修正を加えずにそれが新しいインストーラーで動くことは期待"
"できません。"

#. type: Content of: <chapter><section><section><para>
#: en/installing.dbk:133
msgid ""
"The <ulink url=\"&url-install-manual;\">Installation Guide</ulink> has an "
"updated separate appendix with extensive documentation on using "
"preconfiguration."
msgstr ""
"事前設定の使用方法に関する豊富な文書を含んだ最新の付録が、<ulink url=\"&url-"
"install-manual;\">インストールガイド</ulink>に追加されました。"